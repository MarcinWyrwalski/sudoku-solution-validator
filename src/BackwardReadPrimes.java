import java.util.HashSet;
import java.util.Set;

public class BackwardReadPrimes {

    public static void main(String[] args) {

        int[][] sudoku = new int[9][9];
        boolean flag = false;
        boolean isValueDuplicated = false;
        int entryValueRow = 0;
        int entryValueColumn = 0;
        int sum;


        while (entryValueColumn < sudoku.length && entryValueRow < sudoku.length) {

            Set<Integer> te = new HashSet<>();
            sum = 0;

            for (int i = entryValueColumn; i < entryValueColumn + 3; i++) {
                for (int y = entryValueRow; y < entryValueRow + 3; y++) {

                    te.add(sudoku[i][y]);
                    sum = sum + sudoku[i][y];
                }
            }

            if (te.size() != 9 || sum != 45) {
                flag = true;
            }


            entryValueRow = entryValueRow + 3;
            if (entryValueRow < 9) {

                continue;
            }

            if (entryValueRow == sudoku.length && entryValueColumn < sudoku.length) {
                entryValueRow = 0;
                entryValueColumn = entryValueColumn + 3;
            } else if (entryValueColumn == 9 && entryValueRow < 9) {
                entryValueColumn = 0;
                entryValueRow = entryValueRow + 3;
                continue;
            }

        }

        if (!flag) {

            int value = 0;
            System.out.println(" flaga: " + flag);


            int valueCounter;
            for (int i = 0; i < sudoku.length; i++) {
                valueCounter = 0;
                isValueDuplicated = false;
                value = sudoku[i][0];
                for (int y = 0; y < sudoku[i].length; y++) {
                    if (value == sudoku[i][y]) {

                        if (valueCounter == 0) {
                            valueCounter++;
                            System.out.println("value counter = " + valueCounter);
                            continue;
                        }
                        if (valueCounter > 1) {
                            System.out.println("value counter second if = " + valueCounter);
                            isValueDuplicated = true;
                        }

                    }

                }
            }

            for (int i = 0; i < sudoku.length; i++) {
                valueCounter = 0;
                value = sudoku[0][i];
                isValueDuplicated = false;

                for (int y = 0; y < sudoku[i].length; y++) {

                    if (value == sudoku[i][y]) {
                        if (valueCounter == 0) {
                            valueCounter++;
                        }
                        if (valueCounter < 1) {
                            isValueDuplicated = true;
                        }

                    }
                }


            }
        }

        if (isValueDuplicated) {
            flag = true;
        }
        System.out.println(flag);
    }


}




